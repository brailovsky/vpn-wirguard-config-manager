# VPN Wirguard generator

Locate `~/.config/vpn/wg-keys.yaml`

```bash
# Generate config from configs.vrec record (see VPNRecord class) and save to ./dotfiles
python3 ./wg-config-generator.py --name <name of configs.VPNRecord>

# Generate raw-format config on toplevel of yaml and save to ./dotfiles
python3 ./wg-config-generator.py --absolute-config <name of raw>
```
