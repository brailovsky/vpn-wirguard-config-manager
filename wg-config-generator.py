from ast import parse
from http import client
import os
from typing import List
import toml
import jinja2
from argparse import ArgumentParser
from dataclasses import dataclass

KEYS_PATH = os.path.expanduser('~/.config/vpn/wg-keys.toml')


@dataclass
class VPNRecord:
    Address: str = None
    PublicKey: str = None
    PrivateKey: str = None
    Port: str = None
    ListenPort: str = None
    DNS : str = None
    PresharedKey: str = None
    PersistentKeepalive: str = None
    AllowedIPs: List[str] = None
    dns_search: str = None
    routes: str = None

def generator(peer_name, wg_keys_file=KEYS_PATH, template_file="wg-conf.j2"):
    templateLoader = jinja2.FileSystemLoader(searchpath="./wiregurad/template")
    templateEnv = jinja2.Environment(loader=templateLoader)
    template = templateEnv.get_template(template_file)

    with open(wg_keys_file, 'r') as f:
        cfg = toml.load(f)

    outputText = template.render(dict(
        client=VPNRecord(**cfg['clients'][peer_name]),
        server=VPNRecord(**cfg['server'])
    ))

    print(outputText)
    os.makedirs('/tmp/dotfiles', exist_ok=True)
    with open(f'/tmp/dotfiles/{peer_name}.conf', 'w') as f:
        f.write(outputText)


def absolute_conf_save(peer_name):
    with open(KEYS_PATH, 'r') as f:
        cfg = toml.load(f)
    if peer_name not in cfg.keys():
        print(cfg.keys())
        print(cfg['raw_configs'].keys())
    print(cfg['raw_configs'][peer_name])

    os.makedirs('/tmp/dotfiles', exist_ok=True)
    with open(f'/tmp/dotfiles/{peer_name}.conf', 'w') as f:
        f.write(cfg['raw_configs'][peer_name])


def main():
    parser = ArgumentParser()
    parser.add_argument('--name')
    parser.add_argument('--absolute-config')
    args = parser.parse_args()
    if args.absolute_config:
        absolute_conf_save(args.absolute_config)
    else:
        generator(args.name)


if __name__ == "__main__":
    main()
